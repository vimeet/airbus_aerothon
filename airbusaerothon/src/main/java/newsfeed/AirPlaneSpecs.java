package newsfeed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;

@Entity
public class AirPlaneSpecs {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer msn;

    private Integer harnessLength;

    private Integer grossWeight;

    private Integer atmosphericPressure;

    private Integer fuelCapacityLeftWing;

    private Integer fuelCapacityRightWing;

    public void setMsn(Integer msn) {
        this.msn = msn;
    }

    public void setHarnessLength(Integer harnessLength) {
        this.harnessLength = harnessLength;
    }

    public void setGrossWeight(Integer grossWeight) {
        this.grossWeight = grossWeight;
    }

    public void setAtmosphericPressure(Integer atmosphericPressure) {
        this.atmosphericPressure = atmosphericPressure;
    }

    public void setFuelCapacityLeftWing(Integer fuelCapacityLeftWing) {
        this.fuelCapacityLeftWing = fuelCapacityLeftWing;
    }

    public void setFuelCapacityRightWing(Integer fuelCapacityRightWing) {
        this.fuelCapacityRightWing = fuelCapacityRightWing;
    }


    public Integer getMsn() {
        return msn;
    }

    public Integer getHarnessLength() {
        return harnessLength;
    }

    public Integer getGrossWeight() {
        return grossWeight;
    }

    public Integer getAtmosphericPressure() {
        return atmosphericPressure;
    }

    public Integer getFuelCapacityLeftWing() {
        return fuelCapacityLeftWing;
    }

    public Integer getFuelCapacityRightWing() {
        return fuelCapacityRightWing;
    }


}
