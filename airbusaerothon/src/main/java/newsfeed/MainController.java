package newsfeed;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(path="/airbus")
public class MainController {
    @Autowired
    private NewsFeedRepository newsFeedRepository;

    @Autowired
    private AirPlaneSpecRepository airPlaneSpecRepository;

    @GetMapping(path="/addNews")
    public @ResponseBody String addNewsFeed (@RequestParam String headline
            , @RequestParam String newstext) {

        NewsFeed n = new NewsFeed();
        n.setHeadline(headline);
        n.setNewstext(newstext);
        Date date = new Date();
        n.setDate(date);
        newsFeedRepository.save(n);
        return "Saved";

    }

    @GetMapping(path="/showNews")
    public @ResponseBody Iterable<NewsFeed> getAllNewsFeed() {

        return newsFeedRepository.findAll();
    }

    @RequestMapping(value="/airPlaneSpecs", method = RequestMethod.POST)
    public ResponseEntity<?> addAirplaneSpecs(@RequestBody AirPlaneSpecs airPlaneSpecs) {
        airPlaneSpecRepository.save(airPlaneSpecs);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path="/specs")
    public ResponseEntity getAirplaneSpecs(@RequestParam Integer msn) {

        return new ResponseEntity<>(airPlaneSpecRepository.findById(msn), HttpStatus.OK);
    }

}