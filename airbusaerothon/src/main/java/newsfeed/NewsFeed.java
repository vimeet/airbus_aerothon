package newsfeed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.*;

@Entity // This tells Hibernate to make a table out of this class
public class NewsFeed {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String headline;

    private String newstext;

    private Date date;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public void setNewstext(String newstext) {
        this.newstext = newstext;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public String getHeadline() {
        return headline;
    }

    public String getNewstext() {
        return newstext;
    }

    public Date getDate() {
        return date;
    }




}