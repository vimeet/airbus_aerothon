package newsfeed;

import org.springframework.data.repository.CrudRepository;

import newsfeed.NewsFeed;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface NewsFeedRepository extends CrudRepository<NewsFeed, Integer> {

}